possibilidades = [
	[1,2,3],
	[4,5,6],
	[7,8,9],
	[1,4,7],
	[2,5,8],
	[3,6,9],
	[1,5,9],
	[3,5,7]
];

jogadas = 0;
jogador = 1;
ponto1 = 0;
ponto2 = 0;
controle = false;
ultimo = "";
vencedor = 0;
ganhador = 0;

document.getElementById("vez").innerHTML = "Vez de " + "Goku";


function jogar(casa){
	fundo = document.getElementById(casa);

	if (controle == true){
		
	}
	else{
		if ((fundo.style.backgroundImage == "" || fundo.style.backgroundImage == null)){
			jogadas = jogadas + 1;
			if (jogador == 1){
				fundo.style.backgroundImage = "url(goku.jpg)";
			}
			else{
				fundo.style.backgroundImage = "url(jiren.jpg)";
			}
			verificar();
		}
	}
}

function verificar(){

	for (var i = 0; i < 8; i++){
		casa1 = document.getElementById(possibilidades[i][0]).style.backgroundImage;
		casa2 = document.getElementById(possibilidades[i][1]).style.backgroundImage;
		casa3 = document.getElementById(possibilidades[i][2]).style.backgroundImage;
		if (casa1 == casa2 && casa1 == casa3 && casa1 !== "" && casa1 !== null){
			if (jogador == 1){
				document.getElementById("vez").innerHTML = "Goku" + " ganhou!";
				ponto1 = ponto1 + 1;
				document.getElementById("goku").innerHTML = ponto1;
				vencedor = 1;
				ganhador = 1;
			}
			else{
				document.getElementById("vez").innerHTML = "Jiren" + " ganhou!";
				ponto2 = ponto2 + 1;
				document.getElementById("jiren").innerHTML = ponto2;
				vencedor = 1;
				ganhador = 2;
			}
			controle = true;
		}
	}

	if (controle == false){
		if (jogadas == 9){
			document.getElementById("vez").innerHTML = "Deu velha!";
			document.getElementById("imagemvez").src = "";
			controle = true;
			vencedor = 0;
		}
		else{
			if (jogador == 1){
				jogador = 2;
				document.getElementById("vez").innerHTML = "Vez de " + "Jiren";
				document.getElementById("imagemvez").src = "jiren.jpg";
				ultimo = "Vez de " + "Jiren";
			}
			else{
				jogador = 1;
				document.getElementById("vez").innerHTML = "Vez de " + "Goku";
				document.getElementById("imagemvez").src = "goku.jpg";
				ultimo = "Vez de " + "Goku";
			}
		}		
	}
}

function reiniciar(){
	jogadas = 0;
	controle = false;

	for (var i = 1; i < 10; i++){
		casa = document.getElementById(i);
		casa.style.backgroundImage = "";
	}
	if (vencedor==1){
		vencedor = 0;
		document.getElementById("vez").innerHTML = ultimo;
	}
	if (vencedor==0){
		if(ganhador==1){
			jogador = 1;
			document.getElementById("vez").innerHTML = "Vez de " + "Goku";
			document.getElementById("imagemvez").src = "goku.jpg";
		}
		else if (ganhador==2){
			jogador = 2;
			document.getElementById("vez").innerHTML = "Vez de " + "Jiren";
			document.getElementById("imagemvez").src = "jiren.jpg";
		}
		else if (jogador==1){
			document.getElementById("vez").innerHTML = "Vez de " + "Goku";
			document.getElementById("imagemvez").src = "goku.jpg";
		}
		else{
			document.getElementById("vez").innerHTML = "Vez de " + "Jiren";
			document.getElementById("imagemvez").src = "jiren.jpg";
		}
	}
}

function zeraplacar(){
	reiniciar();
	document.getElementById("vez").innerHTML = "Vez de " + "Goku";
	document.getElementById("imagemvez").src = "goku.jpg";
	document.getElementById("goku").innerHTML = 0;
	document.getElementById("jiren").innerHTML = 0;
	jogadas = 0;
	jogador = 1;
	ponto1 = 0;
	ponto2 = 0;
	controle = false;
	ultimo = "";
	vencedor = 0;
	ganhador = 0;

}